Action()
{
	web_add_auto_header("DNT", 
		"1");

	web_add_auto_header("Upgrade-Insecure-Requests", 
		"1");
	
	web_reg_find("Text=Почта Mail.ru", 
		LAST);	
	
	web_reg_save_param_ex(
		"ParamName=form_sign",
		"LB=name=\"form_sign\" value=\"",
		"RB=\"",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=Yes",
		"RequestUrl=*/inbox*",
		LAST);

	web_reg_save_param_ex(
		"ParamName=form_token",
		"LB=name=\"form_token\" value=\"",
		"RB=\"",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=Yes",
		"RequestUrl=*/inbox*",
		LAST);

	web_reg_save_param_regexp(
		"ParamName=Unread",
		"RegExp=msglist-(.*?)\\\" class=\"messageline messageline_unread",
		"Notfound=warning",
		"Group=1",
		"Ordinal=All",
		SEARCH_FILTERS,
		LAST);

	web_reg_save_param_ex(
		"ParamName=token",
		"LB=name=\"token\" value=\"",
		"RB=\"",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=Yes",
		"RequestUrl=*/inbox*",
		LAST);

	web_url("light.mail.ru", 
		"URL=https://light.mail.ru/", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t513.inf", 
		"Mode=HTML", 
		LAST);
	
	if(atoi(lr_eval_string("{Unread_count}"))==0)
	{
		lr_start_transaction("No_letters_2");
		lr_output_message("Писем неть!");
		lr_end_transaction("No_letters_2",LR_AUTO);
	} 
	else
	{
		
		lr_start_transaction("Read_2");
		Read();
		
	web_submit_data("movemsg",
		"Action=https://light.mail.ru/cgi-bin/movemsg",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=text/html",
		"Referer=https://light.mail.ru/messages/inbox",
		"Snapshot=t882.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=form_sign", "Value={form_sign}", ENDITEM,
		"Name=form_token", "Value={form_token}", ENDITEM,
		"Name=token", "Value={token}", ENDITEM,
		"Name=confirm", "Value=", ENDITEM,
		"Name=page", "Value=1", ENDITEM,
		"Name=back", "Value=/messages/inbox?page=1&sortby=", ENDITEM,
		"Name=folder", "Value=1", ENDITEM,
		"Name=move", "Value=Ok", ENDITEM,
		"Name=markmessage", "Value=0", ENDITEM,
		"Name=id", "Value={UnreadRandom}", ENDITEM,
		LAST);
		
		lr_end_transaction("Read_2",LR_AUTO);
	}
}