Read()
{
	lr_save_string(lr_paramarr_random("Unread"), "UnreadRandom");

	web_reg_find("Text=Входящие - toast.all.test@mail.ru - Почта Mail.ru", 
		LAST);
		
	web_submit_data("movemsg",
		"Action=https://light.mail.ru/cgi-bin/movemsg",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=text/html",
		"Referer=https://light.mail.ru/messages/inbox",
		"Snapshot=t568.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=form_sign", "Value={form_sign}", ENDITEM,
		"Name=form_token", "Value={form_token}", ENDITEM,
		"Name=token", "Value={token}", ENDITEM,
		"Name=confirm", "Value=", ENDITEM,
		"Name=page", "Value=1", ENDITEM,
		"Name=back", "Value=/messages/inbox?page=1&sortby=", ENDITEM,
		"Name=folder", "Value=-1", ENDITEM,
		"Name=markmessage", "Value=2", ENDITEM,
		"Name=mark", "Value=Ok", ENDITEM,
		"Name=id", "Value={UnreadRandom}", ENDITEM,
		LAST);

}