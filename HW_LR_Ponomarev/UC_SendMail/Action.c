Action()
{
	int i;
	lr_start_transaction("UC_SendMail");
	web_add_auto_header("DNT",
		"1");

	web_add_header("Upgrade-Insecure-Requests", 
		"1");
	
		
	srand(time(NULL));
	
	for (i=0;i<3;i++)
	{
		Random_content();
		
		web_reg_save_param_ex(
		"ParamName=Mail_form_sign",
		"LB=name=\"form_sign\" value=\"",
		"RB=\"",
		SEARCH_FILTERS,
		"Scope=Body",
		"RequestUrl=*/compose*",
		LAST);
	
		web_reg_save_param_ex(
		"ParamName=Mail_form_token",
		"LB=name=\"form_token\" value=\"",
		"RB=\"",
		SEARCH_FILTERS,
		"Scope=Body",
		"RequestUrl=*/compose*",
		LAST);
	
	web_url("compose", 
		"URL=https://light.mail.ru/compose/?folder=0", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://light.mail.ru/messages/inbox", 
		"Snapshot=t169.inf",
		"Mode=HTML", 
		LAST);

	web_reg_find("Text=Письмо отправлено - {Mail_login}", 
		LAST);
		
	web_submit_data("compose_2", 
		"Action=https://light.mail.ru/compose/", 
		"Method=POST", 
		"EncType=multipart/form-data", 
		"TargetFrame=", 
		"RecContentType=text/html", 
		"Referer=https://light.mail.ru/compose/?folder=0", 
		"Snapshot=t238.inf", 
		"Mode=HTML", 
		ITEMDATA, 
		"Name=form_sign", "Value={Mail_form_sign}", ENDITEM, 
		"Name=form_token", "Value={Mail_form_token}", ENDITEM, 
		"Name=message", "Value=nfcTtQKc", ENDITEM, 
		"Name=old_charset", "Value=", ENDITEM, 
		"Name=draft_msg", "Value=", ENDITEM, 
		"Name=text", "Value=", ENDITEM, 
		"Name=HTMLMessage", "Value=0", ENDITEM, 
		"Name=htmlencoded", "Value=0", ENDITEM, 
		"Name=template_id", "Value=", ENDITEM, 
		"Name=direction", "Value=re", ENDITEM, 
		"Name=orfo", "Value=rus", ENDITEM, 
		"Name=formessage", "Value=1", ENDITEM, 
		"Name=last_msg_id", "Value=", ENDITEM, 
		"Name=sendauto", "Value=0", ENDITEM, 
		"Name=draftauto", "Value=0", ENDITEM, 
		"Name=as_msg", "Value=", ENDITEM, 
		"Name=clickFrom", "Value=", ENDITEM, 
		"Name=copy", "Value=yes", ENDITEM, 
		"Name=RealName", "Value=0", ENDITEM, 
		"Name=attached_ids", "Value=", ENDITEM, 
		"Name=send", "Value=Отправить", ENDITEM, 
		"Name=To", "Value=\"toast.all.test\" <toast.all.test@mail.ru>", ENDITEM, 
		"Name=CC", "Value=", ENDITEM, 
		"Name=BCC", "Value=", ENDITEM, 
		"Name=Subject", "Value={Subject}", ENDITEM, 
		"Name=File", "Value=", "File=Yes", ENDITEM, 
		"Name=Body", "Value={Body}", ENDITEM, 
		"Name=captcha", "Value=", ENDITEM, 
		LAST);
	}
	lr_end_transaction("UC_SendMail",LR_AUTO);
}