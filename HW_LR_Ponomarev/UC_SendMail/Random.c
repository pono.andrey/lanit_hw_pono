#include <time.h>
#include <stdlib.h>
#include <string.h>

#define SUBJECT_LENGTH 30
#define BODY_LENGTH 100
#define SET "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm123456789 "

Random_content()
{
	int i;
	char subject[SUBJECT_LENGTH];
	char body[BODY_LENGTH];

	for (i = 0; i < SUBJECT_LENGTH-1; i++) {
		subject[i] = SET[rand()%strlen(SET)];
	}
		
	subject[i] = '\0';

	lr_save_string(&*subject, "Subject");
	
	for (i = 0; i < BODY_LENGTH-1; i++) {
		body[i] = SET[rand()%strlen(SET)];
	}
		
	body[i] = '\0';

	lr_save_string(&*body, "Body");
}